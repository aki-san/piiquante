const path = require('path');
const userRoutes = require('./routes/users');
const sauceRoutes = require('./routes/sauces');
const express = require('express');
const mongoose = require('mongoose');
const rateLimit = require('express-rate-limit');
const helmet = require('helmet');
const app = express();

// Connexion à MongoDB
mongoose.connect(process.env.MONGO_URI,
  { useNewUrlParser: true,
    useUnifiedTopology: true })
  .then(() => console.log('Connection to MongoDB successful !'))
  .catch(() => console.log('Connection to MongoDB failed !'));

// Conversion de la variable d'environnement RATE_LIMIT_WINDOW en millisecondes
const windowMs = parseInt(process.env.RATE_LIMIT_WINDOW || '15') * 60 * 1000;
const max = parseInt(process.env.RATE_LIMIT_MAX_REQUESTS || '100');

const limiter = rateLimit({
  windowMs: windowMs, // 15 minutes
  max: max, // limitation pour chaque IP à 100 requêtes par windowMs
  message: "Too many requests sent from this IP, please try again later."
});

// Application du middleware pour toutes les requêtes
app.use(limiter);

// Middleware pour parser les données JSON
app.use(express.json());

// Middleware pour les en-têtes CORS
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', req.headers.origin || '*'); // req.headers.origin : pas ouf pour la sécurité
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  res.setHeader('Access-Control-Allow-Credentials', 'true');
  next();
});

app.use(
  helmet({
    contentSecurityPolicy: {
      directives: {
        defaultSrc: ["'self'"],
        imgSrc: ["'self'", 'data:'],
        scriptSrc: ["'self'"],
        styleSrc: ["'self'"]
      }
    },
    crossOriginResourcePolicy: { policy: "cross-origin" },
    crossOriginEmbedderPolicy: false,
  })
);

// Middleware pour servir les fichiers statiques du dossier 'images'
app.use('/images', express.static(path.join(__dirname, 'images')));

// Gestion des routes
app.use('/api/sauces', sauceRoutes);
app.use('/api/auth', userRoutes);

// Export de l'application pour server.js
module.exports = app;
