const jwt = require('jsonwebtoken');
 
module.exports = (req, res, next) => {
   try {
       const token = req.headers.authorization.split(' ')[1];
       const decodedToken = jwt.verify(token, process.env.JWT_SECRET);
       const userId = decodedToken.userId;
       req.auth = {
           userId: userId
       };
	next();
  } catch(error) {
    console.error(error);
    if (error instanceof jwt.JsonWebTokenError) {
        res.status(401).json({ message: 'Error: invalid or expired token' });
    } else {
        res.status(500).json({ message: 'Unexpected error occurred' });
    }
  }
};
