const Sauce = require('../models/Sauce');
const fs = require('fs');

exports.createSauce = async (req, res) => {
  try {
    const sauceObject = JSON.parse(req.body.sauce);
    delete sauceObject._id;
    delete sauceObject._userId;
    const sauce = new Sauce({
      ...sauceObject,
      userId: req.auth.userId,
      imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
    });

    await sauce.save();
    res.status(201).json({ message: 'Sauce created!' });
  } catch (error) {
    console.error(error);
    if (error instanceof mongoose.Error) {
        res.status(500).json({ message: 'Database error: failed to retrieve sauce' });
    } else {
        res.status(500).json({ message: 'Unexpected error occurred' });
    }
  }
};

exports.getOneSauce = async (req, res) => {
  try {
    const sauce = await Sauce.findOne({
      _id: req.params.id
    });
    res.status(200).json(sauce);
  } catch (error) {
    console.error(error);
    if (error instanceof mongoose.Error) {
        res.status(500).json({ message: 'Database error: failed to retrieve sauce' });
    } else {
        res.status(500).json({ message: 'Unexpected error occurred' });
    }
  }
};

exports.modifySauce = async (req, res) => {
  try {
    const sauceObject = req.file ? {
      ...JSON.parse(req.body.sauce),
      imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
    } : { ...req.body };

    delete sauceObject._userId;
    const sauce = await Sauce.findOne({ _id: req.params.id });

    if (sauce.userId !== req.auth.userId) {
      res.status(401).json({ message: 'Not authorized' });
    } else {
      await Sauce.updateOne({ _id: req.params.id }, { $set: sauceObject });
      res.status(200).json({ message: 'Sauce modified!' });
    }
  } catch (error) {
    console.error(error);
    if (error instanceof mongoose.Error) {
        res.status(500).json({ message: 'Database error: failed to retrieve sauce' });
    } else {
        res.status(500).json({ message: 'Unexpected error occurred' });
    }
  }
};

exports.deleteSauce = async (req, res) => {
  try {
    const sauce = await Sauce.findOne({ _id: req.params.id });

    if (!sauce) {
      return res.status(404).json({ message: 'Sauce not found!' });
    }

    if (sauce.userId !== req.auth.userId) {
      res.status(401).json({ message: 'Not authorized' });
    } else {
      const filename = sauce.imageUrl.split('/images/')[1];
      fs.unlink(`images/${filename}`, async () => {
        await Sauce.deleteOne({ _id: req.params.id });
        res.status(200).json({ message: 'Sauce deleted !' });
      });
    }
  } catch (error) {
    console.error(error);
    if (error instanceof mongoose.Error) {
        res.status(500).json({ message: 'Database error: failed to retrieve sauce' });
    } else {
        res.status(500).json({ message: 'Unexpected error occurred' });
    }
  }
};

exports.getAllSauces = async (req, res) => {
  try {
    const sauces = await Sauce.find();
    res.status(200).json(sauces);
  } catch (error) {
    console.error(error);
    if (error instanceof mongoose.Error) {
        res.status(500).json({ message: 'Database error: failed to retrieve sauce' });
    } else {
        res.status(500).json({ message: 'Unexpected error occurred' });
    }
  }
};