const Sauce = require('../models/Sauce');

const updateLikesDislikes = async (id, userId, likeChange, dislikeChange) => {
  await Sauce.updateOne(
    { _id: id },
    {
      $inc: { likes: likeChange, dislikes: dislikeChange },
      $push: {
        usersLiked: likeChange === 1 ? userId : undefined,
        usersDisliked: dislikeChange === 1 ? userId : undefined
      },
      $pull: {
        usersLiked: likeChange === -1 ? userId : undefined,
        usersDisliked: dislikeChange === -1 ? userId : undefined
      }
    }
  );
};

const isAlreadyLiked = (body, sauce) => {
  return body.like === 1 && !sauce.usersLiked.includes(body.userId);
};

const isAlreadyDisliked = (body, sauce) => {
  return body.like === -1 && !sauce.usersDisliked.includes(body.userId);
};

function isRequestValid(like) {
  return ![-1, 0, 1].includes(like);
}

function isCancelRequest(like) {
  return like === 0;
}

exports.likeDislikeSauce = async (req, res) => {
  try {
    const sauce = await Sauce.findOne({ _id: req.params.id });
    let likeChange = 0;
    let dislikeChange = 0;
    let message = '';

    if(isRequestValid(req.body.like)){
      return res.status(400).json({ message: 'Invalid like value!' });
    }

    if(req.body.like === 1) {
      if (isAlreadyLiked(req.body, sauce)) {
        likeChange = 1;
        message = 'Like added!';
      }
    } else if (req.body.like === -1) {
      if (isAlreadyDisliked(req.body, sauce)) {
        dislikeChange = 1;
        message = 'Dislike added!';
      }
    }

    if(isCancelRequest(req.body.like)) {
      if(sauce.usersLiked.includes(req.body.userId)) {
        likeChange = -1;
        message = 'Like removed!';
      } else if(sauce.usersDisliked.includes(req.body.userId)) {
        dislikeChange = -1;
        message = 'Dislike removed!';
      }
    }

    await updateLikesDislikes(req.params.id, req.body.userId, likeChange, dislikeChange);
    res.status(200).json({ message });

  } catch (error) {
    res.status(400).json({ error });
  }
};
