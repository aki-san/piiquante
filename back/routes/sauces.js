const express = require('express');
const router = express.Router();
const { body } = require('express-validator');

const auth = require('../middleware/auth');
const multer = require('../middleware/multer-config');

const saucesCtrl = require('../controllers/sauce');
const likesCtrl = require('../controllers/like');

router.get('/', auth, saucesCtrl.getAllSauces);
router.post('/', auth, multer,
  [
    body('sauce')
      .custom((value, { req }) => {
        const sauce = JSON.parse(value);
        if (!sauce.name) throw new Error('Name required');
        if (!sauce.manufacturer) throw new Error('Manufacturer required');
        if (!sauce.description) throw new Error('Description required');
        if (!sauce.mainPepper) throw new Error('Main pepper ingredient required');
        if (sauce.heat === undefined || sauce.heat < 0 || sauce.heat > 10) throw new Error('Invalid heat rating');
        return true;
      })
  ],
  saucesCtrl.createSauce
);
router.post('/:id/like', auth, likesCtrl.likeDislikeSauce);
router.get('/:id', auth, saucesCtrl.getOneSauce);
router.put('/:id', auth, multer,
  [
    body('sauce')
      .optional()
      .custom((value, { req }) => {
        if (value) {
          const sauce = JSON.parse(value);
          if (!sauce.name) throw new Error('Name required');
          if (!sauce.manufacturer) throw new Error('Manufacturer required');
          if (!sauce.description) throw new Error('Description required');
          if (!sauce.mainPepper) throw new Error('Main pepper ingredient required');
          if (sauce.heat === undefined || sauce.heat < 0 || sauce.heat > 10) throw new Error('Invalid heat rating');
        }
        return true;
      })
  ],
  saucesCtrl.modifySauce
);
router.delete('/:id', auth, saucesCtrl.deleteSauce);

module.exports = router;