const express = require('express');
const router = express.Router();
const { body } = require('express-validator');

const userCtrl = require('../controllers/user');

router.post('/signup',
  [
    body('email')
      .isEmail()
      .withMessage('Please enter a valid email address'),
    body('password')
      .isLength({ min: 8 })
      .withMessage('Password must be at least 8 characters long')
  ],
  userCtrl.signup
);

router.post('/login', userCtrl.login);

module.exports = router;